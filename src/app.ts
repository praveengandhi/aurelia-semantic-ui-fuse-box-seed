import { Router, RouterConfiguration } from 'aurelia-router';
import {PLATFORM} from 'aurelia-pal';
export class App {
    public router: Router;

    public configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Aurelia';
        config.map([
            { route: ['', 'welcome'], name: 'welcome',      moduleId: PLATFORM.moduleName('./routes/welcome'),      nav: true, title: 'App' },
            { route: 'child-router',  name: 'child-router', moduleId: PLATFORM.moduleName('./routes/child-router'), nav: true, title: 'Child Router' }
        ]);
        this.router = router;
    }
}
