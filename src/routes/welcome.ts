// import {computedFrom} from 'aurelia-framework';

export class Welcome {
    public heading = 'Aurelia App';
    public firstName = 'John';
    public lastName = 'Doe';
    public previousValue: string = this.fullName;

    // Getters can't be directly observed, so they must be dirty checked.
    // However, if you tell Aurelia the dependencies, it no longer needs to dirty check the property.
    // To optimize by declaring the properties that this getter is computed from, uncomment the line below
    // as well as the corresponding import above.
    // @computedFrom('firstName', 'lastName')
    public get fullName(): string {
      return `${this.firstName} ${this.lastName}`;
    }

    public submit() {
      this.previousValue = this.fullName;
      alert(`Welcome, ${this.fullName}!`);
    }

    public canDeactivate(): boolean | undefined {
      if (this.fullName !== this.previousValue) {
        return confirm('Are you sure you want to leave?');
      }
      return true;
    }
}

export class UpperValueConverter {
    public toView(value: string): string {
        return value && value.toUpperCase();
    }
}
