# aurelia + semantic ui + fuse-box

## How to install:
```npm install```  
```cd semantic```  
```gulp build```  
```cd ..```

## How to run:
```npm run start```

## How to build without hmr (used in live demo):
```npm run demo```

## Loader used:
[fuse-box-aurelia-loader](https://github.com/fuse-box/fuse-box-aurelia-loader)

## Seed/Skeleton used:
[fuse-box-aurelia-seed](https://github.com/fuse-box/fuse-box-aurelia-seed)

## Aurelia Semantic UI
[aurelia semantic ui](https://bitbucket.org/ged/aurelia-semantic-ui/)